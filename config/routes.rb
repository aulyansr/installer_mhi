Rails.application.routes.draw do

  root to: 'page#homepage'

  resources :oudoor_units
  resources :indoor_units
  resources :point_details
  resources :points
  devise_for :users
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
