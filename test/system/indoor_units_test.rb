require "application_system_test_case"

class IndoorUnitsTest < ApplicationSystemTestCase
  setup do
    @indoor_unit = indoor_units(:one)
  end

  test "visiting the index" do
    visit indoor_units_url
    assert_selector "h1", text: "Indoor Units"
  end

  test "creating a Indoor unit" do
    visit indoor_units_url
    click_on "New Indoor Unit"

    fill_in "Point", with: @indoor_unit.point
    fill_in "Title", with: @indoor_unit.title
    click_on "Create Indoor unit"

    assert_text "Indoor unit was successfully created"
    click_on "Back"
  end

  test "updating a Indoor unit" do
    visit indoor_units_url
    click_on "Edit", match: :first

    fill_in "Point", with: @indoor_unit.point
    fill_in "Title", with: @indoor_unit.title
    click_on "Update Indoor unit"

    assert_text "Indoor unit was successfully updated"
    click_on "Back"
  end

  test "destroying a Indoor unit" do
    visit indoor_units_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Indoor unit was successfully destroyed"
  end
end
