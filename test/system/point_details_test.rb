require "application_system_test_case"

class PointDetailsTest < ApplicationSystemTestCase
  setup do
    @point_detail = point_details(:one)
  end

  test "visiting the index" do
    visit point_details_url
    assert_selector "h1", text: "Point Details"
  end

  test "creating a Point detail" do
    visit point_details_url
    click_on "New Point Detail"

    fill_in "Image", with: @point_detail.image
    fill_in "Point", with: @point_detail.point_id
    fill_in "Title", with: @point_detail.title
    click_on "Create Point detail"

    assert_text "Point detail was successfully created"
    click_on "Back"
  end

  test "updating a Point detail" do
    visit point_details_url
    click_on "Edit", match: :first

    fill_in "Image", with: @point_detail.image
    fill_in "Point", with: @point_detail.point_id
    fill_in "Title", with: @point_detail.title
    click_on "Update Point detail"

    assert_text "Point detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Point detail" do
    visit point_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Point detail was successfully destroyed"
  end
end
