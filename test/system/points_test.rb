require "application_system_test_case"

class PointsTest < ApplicationSystemTestCase
  setup do
    @point = points(:one)
  end

  test "visiting the index" do
    visit points_url
    assert_selector "h1", text: "Points"
  end

  test "creating a Point" do
    visit points_url
    click_on "New Point"

    fill_in "Image verification", with: @point.image_verification
    fill_in "Indoor unit", with: @point.indoor_unit_id
    fill_in "Outdoor unit", with: @point.outdoor_unit_id
    fill_in "Point", with: @point.point
    fill_in "Serial number outdoor", with: @point.serial_number_outdoor
    fill_in "Serial number outdoor image", with: @point.serial_number_outdoor_image
    fill_in "Serial number unit indoor", with: @point.serial_number_unit_indoor
    fill_in "Serial number unit indoor image", with: @point.serial_number_unit_indoor_image
    fill_in "Status", with: @point.status
    fill_in "User", with: @point.user_id
    fill_in "Warranty book image", with: @point.warranty_book_image
    click_on "Create Point"

    assert_text "Point was successfully created"
    click_on "Back"
  end

  test "updating a Point" do
    visit points_url
    click_on "Edit", match: :first

    fill_in "Image verification", with: @point.image_verification
    fill_in "Indoor unit", with: @point.indoor_unit_id
    fill_in "Outdoor unit", with: @point.outdoor_unit_id
    fill_in "Point", with: @point.point
    fill_in "Serial number outdoor", with: @point.serial_number_outdoor
    fill_in "Serial number outdoor image", with: @point.serial_number_outdoor_image
    fill_in "Serial number unit indoor", with: @point.serial_number_unit_indoor
    fill_in "Serial number unit indoor image", with: @point.serial_number_unit_indoor_image
    fill_in "Status", with: @point.status
    fill_in "User", with: @point.user_id
    fill_in "Warranty book image", with: @point.warranty_book_image
    click_on "Update Point"

    assert_text "Point was successfully updated"
    click_on "Back"
  end

  test "destroying a Point" do
    visit points_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Point was successfully destroyed"
  end
end
