require "application_system_test_case"

class OudoorUnitsTest < ApplicationSystemTestCase
  setup do
    @oudoor_unit = oudoor_units(:one)
  end

  test "visiting the index" do
    visit oudoor_units_url
    assert_selector "h1", text: "Oudoor Units"
  end

  test "creating a Oudoor unit" do
    visit oudoor_units_url
    click_on "New Oudoor Unit"

    fill_in "Point", with: @oudoor_unit.point
    fill_in "Title", with: @oudoor_unit.title
    click_on "Create Oudoor unit"

    assert_text "Oudoor unit was successfully created"
    click_on "Back"
  end

  test "updating a Oudoor unit" do
    visit oudoor_units_url
    click_on "Edit", match: :first

    fill_in "Point", with: @oudoor_unit.point
    fill_in "Title", with: @oudoor_unit.title
    click_on "Update Oudoor unit"

    assert_text "Oudoor unit was successfully updated"
    click_on "Back"
  end

  test "destroying a Oudoor unit" do
    visit oudoor_units_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Oudoor unit was successfully destroyed"
  end
end
