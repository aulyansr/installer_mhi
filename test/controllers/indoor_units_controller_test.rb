require 'test_helper'

class IndoorUnitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @indoor_unit = indoor_units(:one)
  end

  test "should get index" do
    get indoor_units_url
    assert_response :success
  end

  test "should get new" do
    get new_indoor_unit_url
    assert_response :success
  end

  test "should create indoor_unit" do
    assert_difference('IndoorUnit.count') do
      post indoor_units_url, params: { indoor_unit: { point: @indoor_unit.point, title: @indoor_unit.title } }
    end

    assert_redirected_to indoor_unit_url(IndoorUnit.last)
  end

  test "should show indoor_unit" do
    get indoor_unit_url(@indoor_unit)
    assert_response :success
  end

  test "should get edit" do
    get edit_indoor_unit_url(@indoor_unit)
    assert_response :success
  end

  test "should update indoor_unit" do
    patch indoor_unit_url(@indoor_unit), params: { indoor_unit: { point: @indoor_unit.point, title: @indoor_unit.title } }
    assert_redirected_to indoor_unit_url(@indoor_unit)
  end

  test "should destroy indoor_unit" do
    assert_difference('IndoorUnit.count', -1) do
      delete indoor_unit_url(@indoor_unit)
    end

    assert_redirected_to indoor_units_url
  end
end
