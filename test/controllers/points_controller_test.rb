require 'test_helper'

class PointsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @point = points(:one)
  end

  test "should get index" do
    get points_url
    assert_response :success
  end

  test "should get new" do
    get new_point_url
    assert_response :success
  end

  test "should create point" do
    assert_difference('Point.count') do
      post points_url, params: { point: { image_verification: @point.image_verification, indoor_unit_id: @point.indoor_unit_id, outdoor_unit_id: @point.outdoor_unit_id, point: @point.point, serial_number_outdoor: @point.serial_number_outdoor, serial_number_outdoor_image: @point.serial_number_outdoor_image, serial_number_unit_indoor: @point.serial_number_unit_indoor, serial_number_unit_indoor_image: @point.serial_number_unit_indoor_image, status: @point.status, user_id: @point.user_id, warranty_book_image: @point.warranty_book_image } }
    end

    assert_redirected_to point_url(Point.last)
  end

  test "should show point" do
    get point_url(@point)
    assert_response :success
  end

  test "should get edit" do
    get edit_point_url(@point)
    assert_response :success
  end

  test "should update point" do
    patch point_url(@point), params: { point: { image_verification: @point.image_verification, indoor_unit_id: @point.indoor_unit_id, outdoor_unit_id: @point.outdoor_unit_id, point: @point.point, serial_number_outdoor: @point.serial_number_outdoor, serial_number_outdoor_image: @point.serial_number_outdoor_image, serial_number_unit_indoor: @point.serial_number_unit_indoor, serial_number_unit_indoor_image: @point.serial_number_unit_indoor_image, status: @point.status, user_id: @point.user_id, warranty_book_image: @point.warranty_book_image } }
    assert_redirected_to point_url(@point)
  end

  test "should destroy point" do
    assert_difference('Point.count', -1) do
      delete point_url(@point)
    end

    assert_redirected_to points_url
  end
end
