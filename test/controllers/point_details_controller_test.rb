require 'test_helper'

class PointDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @point_detail = point_details(:one)
  end

  test "should get index" do
    get point_details_url
    assert_response :success
  end

  test "should get new" do
    get new_point_detail_url
    assert_response :success
  end

  test "should create point_detail" do
    assert_difference('PointDetail.count') do
      post point_details_url, params: { point_detail: { image: @point_detail.image, point_id: @point_detail.point_id, title: @point_detail.title } }
    end

    assert_redirected_to point_detail_url(PointDetail.last)
  end

  test "should show point_detail" do
    get point_detail_url(@point_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_point_detail_url(@point_detail)
    assert_response :success
  end

  test "should update point_detail" do
    patch point_detail_url(@point_detail), params: { point_detail: { image: @point_detail.image, point_id: @point_detail.point_id, title: @point_detail.title } }
    assert_redirected_to point_detail_url(@point_detail)
  end

  test "should destroy point_detail" do
    assert_difference('PointDetail.count', -1) do
      delete point_detail_url(@point_detail)
    end

    assert_redirected_to point_details_url
  end
end
