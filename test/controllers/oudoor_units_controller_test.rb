require 'test_helper'

class OudoorUnitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @oudoor_unit = oudoor_units(:one)
  end

  test "should get index" do
    get oudoor_units_url
    assert_response :success
  end

  test "should get new" do
    get new_oudoor_unit_url
    assert_response :success
  end

  test "should create oudoor_unit" do
    assert_difference('OudoorUnit.count') do
      post oudoor_units_url, params: { oudoor_unit: { point: @oudoor_unit.point, title: @oudoor_unit.title } }
    end

    assert_redirected_to oudoor_unit_url(OudoorUnit.last)
  end

  test "should show oudoor_unit" do
    get oudoor_unit_url(@oudoor_unit)
    assert_response :success
  end

  test "should get edit" do
    get edit_oudoor_unit_url(@oudoor_unit)
    assert_response :success
  end

  test "should update oudoor_unit" do
    patch oudoor_unit_url(@oudoor_unit), params: { oudoor_unit: { point: @oudoor_unit.point, title: @oudoor_unit.title } }
    assert_redirected_to oudoor_unit_url(@oudoor_unit)
  end

  test "should destroy oudoor_unit" do
    assert_difference('OudoorUnit.count', -1) do
      delete oudoor_unit_url(@oudoor_unit)
    end

    assert_redirected_to oudoor_units_url
  end
end
