class OudoorUnitsController < ApplicationController
  before_action :set_oudoor_unit, only: %i[ show edit update destroy ]

  # GET /oudoor_units or /oudoor_units.json
  def index
    @oudoor_units = OudoorUnit.all
  end

  # GET /oudoor_units/1 or /oudoor_units/1.json
  def show
  end

  # GET /oudoor_units/new
  def new
    @oudoor_unit = OudoorUnit.new
  end

  # GET /oudoor_units/1/edit
  def edit
  end

  # POST /oudoor_units or /oudoor_units.json
  def create
    @oudoor_unit = OudoorUnit.new(oudoor_unit_params)

    respond_to do |format|
      if @oudoor_unit.save
        format.html { redirect_to @oudoor_unit, notice: "Oudoor unit was successfully created." }
        format.json { render :show, status: :created, location: @oudoor_unit }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @oudoor_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /oudoor_units/1 or /oudoor_units/1.json
  def update
    respond_to do |format|
      if @oudoor_unit.update(oudoor_unit_params)
        format.html { redirect_to @oudoor_unit, notice: "Oudoor unit was successfully updated." }
        format.json { render :show, status: :ok, location: @oudoor_unit }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @oudoor_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /oudoor_units/1 or /oudoor_units/1.json
  def destroy
    @oudoor_unit.destroy
    respond_to do |format|
      format.html { redirect_to oudoor_units_url, notice: "Oudoor unit was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_oudoor_unit
      @oudoor_unit = OudoorUnit.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def oudoor_unit_params
      params.require(:oudoor_unit).permit(:title, :point)
    end
end
