class PointDetailsController < ApplicationController
  before_action :set_point_detail, only: %i[ show edit update destroy ]

  # GET /point_details or /point_details.json
  def index
    @point_details = PointDetail.all
  end

  # GET /point_details/1 or /point_details/1.json
  def show
  end

  # GET /point_details/new
  def new
    @point_detail = PointDetail.new
  end

  # GET /point_details/1/edit
  def edit
  end

  # POST /point_details or /point_details.json
  def create
    @point_detail = PointDetail.new(point_detail_params)

    respond_to do |format|
      if @point_detail.save
        format.html { redirect_to @point_detail, notice: "Point detail was successfully created." }
        format.json { render :show, status: :created, location: @point_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @point_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /point_details/1 or /point_details/1.json
  def update
    respond_to do |format|
      if @point_detail.update(point_detail_params)
        format.html { redirect_to @point_detail, notice: "Point detail was successfully updated." }
        format.json { render :show, status: :ok, location: @point_detail }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @point_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /point_details/1 or /point_details/1.json
  def destroy
    @point_detail.destroy
    respond_to do |format|
      format.html { redirect_to point_details_url, notice: "Point detail was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_point_detail
      @point_detail = PointDetail.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def point_detail_params
      params.require(:point_detail).permit(:point_id, :image, :title)
    end
end
