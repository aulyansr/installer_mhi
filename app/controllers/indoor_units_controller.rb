class IndoorUnitsController < ApplicationController
  before_action :set_indoor_unit, only: %i[ show edit update destroy ]

  # GET /indoor_units or /indoor_units.json
  def index
    @indoor_units = IndoorUnit.all
  end

  # GET /indoor_units/1 or /indoor_units/1.json
  def show
  end

  # GET /indoor_units/new
  def new
    @indoor_unit = IndoorUnit.new
  end

  # GET /indoor_units/1/edit
  def edit
  end

  # POST /indoor_units or /indoor_units.json
  def create
    @indoor_unit = IndoorUnit.new(indoor_unit_params)

    respond_to do |format|
      if @indoor_unit.save
        format.html { redirect_to @indoor_unit, notice: "Indoor unit was successfully created." }
        format.json { render :show, status: :created, location: @indoor_unit }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @indoor_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /indoor_units/1 or /indoor_units/1.json
  def update
    respond_to do |format|
      if @indoor_unit.update(indoor_unit_params)
        format.html { redirect_to @indoor_unit, notice: "Indoor unit was successfully updated." }
        format.json { render :show, status: :ok, location: @indoor_unit }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @indoor_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /indoor_units/1 or /indoor_units/1.json
  def destroy
    @indoor_unit.destroy
    respond_to do |format|
      format.html { redirect_to indoor_units_url, notice: "Indoor unit was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_indoor_unit
      @indoor_unit = IndoorUnit.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def indoor_unit_params
      params.require(:indoor_unit).permit(:title, :point)
    end
end
