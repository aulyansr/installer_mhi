json.extract! point_detail, :id, :point_id, :image, :title, :created_at, :updated_at
json.url point_detail_url(point_detail, format: :json)
