json.extract! user, :id, :name, :phone_number, :id_number, :address, :city, :image, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
