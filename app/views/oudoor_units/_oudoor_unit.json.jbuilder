json.extract! oudoor_unit, :id, :title, :point, :created_at, :updated_at
json.url oudoor_unit_url(oudoor_unit, format: :json)
