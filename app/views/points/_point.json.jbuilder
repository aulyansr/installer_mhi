json.extract! point, :id, :user_id, :warranty_book_image, :serial_number_unit_indoor, :serial_number_unit_indoor_image, :serial_number_outdoor, :serial_number_outdoor_image, :image_verification, :point, :status, :indoor_unit_id, :outdoor_unit_id, :created_at, :updated_at
json.url point_url(point, format: :json)
