json.extract! indoor_unit, :id, :title, :point, :created_at, :updated_at
json.url indoor_unit_url(indoor_unit, format: :json)
