# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_30_233408) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "indoor_units", force: :cascade do |t|
    t.string "title"
    t.string "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oudoor_units", force: :cascade do |t|
    t.string "title"
    t.string "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "point_details", force: :cascade do |t|
    t.bigint "point_id"
    t.string "image"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["point_id"], name: "index_point_details_on_point_id"
  end

  create_table "points", force: :cascade do |t|
    t.bigint "user_id"
    t.string "warranty_book_image"
    t.string "serial_number_unit_indoor"
    t.string "serial_number_unit_indoor_image"
    t.string "serial_number_outdoor"
    t.string "serial_number_outdoor_image"
    t.string "image_verification"
    t.integer "point"
    t.string "status"
    t.integer "indoor_unit_id"
    t.integer "outdoor_unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_points_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.string "id_number"
    t.text "address"
    t.string "city"
    t.string "image"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "point_details", "points"
  add_foreign_key "points", "users"
end
