class CreateIndoorUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :indoor_units do |t|
      t.string :title
      t.string :point

      t.timestamps
    end
  end
end
