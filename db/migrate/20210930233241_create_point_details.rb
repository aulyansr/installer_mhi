class CreatePointDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :point_details do |t|
      t.references :point, foreign_key: true
      t.string :image
      t.string :title

      t.timestamps
    end
  end
end
