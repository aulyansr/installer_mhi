class CreatePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.references :user, foreign_key: true
      t.string :warranty_book_image
      t.string :serial_number_unit_indoor
      t.string :serial_number_unit_indoor_image
      t.string :serial_number_outdoor
      t.string :serial_number_outdoor_image
      t.string :image_verification
      t.integer :point
      t.string :status
      t.integer :indoor_unit_id
      t.integer :outdoor_unit_id

      t.timestamps
    end
  end
end
